# Summer 2020 Internsip Project

## Issue 4 - Summary of Articles Read

### 2015 - A Review on Generative Adversarial Networks - Algorithms Theory and Applications

GAN are designed to reach the Nash equilibrium. GAN are a direct implicit model. In a GAN, the Generator and the Discriminator compete against each other and "learn" from attempting to out perform the other. The generator attempts to create fake data, and the discriminator attempts to determine if the data received is fake or real. There are dozens of different types of GAN's, each attempting to overcome issues with previous iterations, or to solve a particular problem. Some involve the use of an encoder or an autoencoder, which takes the input and transforms it.
There are various methods for determining the effectiveness or accuracy of a gan. These are mode score, Freched Inception Distance, and Multi-scale structural similarity.
GAN's can be used for things like image processing, predicting the next frame in a video, or language progressing, such as text to speech. More advanced GAN's can create photo realistic images from text or sketches.

### 2016 - NIPS 2016 Tutorial - Generative Adversarial Networks Small

This paper describes further the function and use of GAN's. GAN's are perfect for choosing a single output when there are many correct answers. As described above, GAN's work by having a generator create fake data and pass that along to the discriminator, whose job it is to determine whether the sample is real or fake. The discriminator then tells the generator it's findings and the process repeats.
The paper then describes the taxonomy of deep generative models, and where GAN's fall on this scale. In the examples all GAN's are assumed to use Maximum Likelihood, as it makes comparison to other methods easier. GAN's use Implicit Density.
They then talk about the cost functions & training process. A smaller gradient for the cost the less the generator learns. They also continue to talk about how GAN's were designed to be more flexible than past methods and to have very little constraints.
They also discus Mode Collapse, which is a situation in which the generator choose individual details as opposed to the larger picture, such as coloring a single square of a checkerboard, and moving the location of the square as the discriminators decides it is fake, vs the overall pattern of light and dark.
The 4th section includes various tips for working with a GAN, such as training with labels, which vastly decreases the training time. They also talk about using one-sided label smoothing and batch normalization.
Then they give several options for further research in areas of GAN's that are not well understood. These areas include non-convergence, mode collapse
Finally, they discuss the evolution of generative models, and provide sample questions and answers for the reader to work though.

### 2019 - Coevolution of Generative Adversarial Networks

This paper proposes a new model coevolutionary generative adversarial networks (COEGAN). In this model they combine neuroevoltuion and coevolution into a GAN. The goal of this was to attempt to overcome some of the issues with GAN's, mainly mode collapse which is described above, and vanishing gradient. When the discriminator becomes very good or perfect at determining whether a sample is real or fake, the gradient vanishes, meaning the generator has no new information to learn from, and the process stagnates.
To overcome these limitations, they propose combining NeuroEvolution of Augmentation Topologies (NEAT) with existing GAN's. The result allows the model change and evolve as the generator learns. The genotype of the generator and discriminator ends up being a combination of layers and activation functions. They selected the all vs best method for pairing the generators with the discriminators. This method puts the best k generators against all discriminators and vis versa.
The scope of their experiment was limited by computation, but their results showed their new model reduced the mode collapse problem. The reused genes continued to increase through the generations and the layers quickly reached it peak, indicating the limit must be increased or the growth rate must be slowed. While the FID for the generator was not as good as state of the art methods, it still did not have a mode collapse. The constant evolution also reduced the vanishing gradient problem.

### 2019 - Evolutionary Generative Adversarial Networks

This paper proposes a new Evolutionary GAN (EGAN). Where traditional GAN's have fixed training objectives such as Jensen–Shannon divergence (JSD), least squares, absolute deviation, Kullback–Leibler (KL) divergence , and Wasserstein distance. Each of these metrics have their own downsides and drawbacks though. None of the metrics can avoid vanishing gradient, mode collapse, and non-convergent limit cycles. In an EGAN, multiple Generators are produced to compete with each other, with the best being allowed to reproduce and evolve, while the worst are "killed". In this way, the generator is allowed to change its objective over time, resulting in the best method at each stage of training. This results in longer times per step, but less steps required to train to a similar accuracy, as well as a higher overall accuracy and reducing the majority of the previously mentioned issues.

Two new metrics were created to determine how well the Generators were performing against each other. The first is accuracy, which is how well they are able to fool the discriminator. The second is the diversity, or whether the samples are spread far enough to avoid mode collapse.

They tested their new EGAN on 3 different datasets, and in each one the EGAN produced better data and avoided many of the problems present in previous methods. The new EGAN produced more consistent results, more stable training, and a better final generator than previous implementations.

### 2019 Neuroevolutionary Training of Deep Convolutional Generative Adversarial Networks

This paper proposes two new GAN's, GAGAN and CAGAN, which take EGAN(discussed above) and expand upon the concept to produce a Generator and Discriminator that evolves as the training progresses. The paper is broken down into 7 different chapters.

The first chapter discusses the overview of GAN's, (covered in sufficient detail above) as well as the specific problem they aim to address. The new GAN's proposed in this paper aim to solve mode collapse and vanishing gradient having to do with small sample sizes, as is the case with some real world data. An additional issue that they aim to solve is to reduce the human effort and time needed to optimize the hyper-parameters by trial and error, by allowing the network to evolve on its own as its training.

The 2nd chapter discusses background on the creation of GAN's and how they work down to the base components, such as perceptrons, which are the neurons, the activation and loss functions, and gradient descent. All of these are hyper-parameters that must be chosen ahead of time with traditional GAN's. This decision is what can make or break the performance of a GAN. They then talk about CNN's as these are the Discriminator for a GAN, while the generator is a reverse CNN. Finally, they discuss GAN's and genetic/cultural algorithms, which are the main focus of this paper.

Chapter 3 discussed related literature, some of which were described above, such as EGAN. They also discuss the topology of GAN's and different types such as COEGAN and its inspiration, NEAT (Neuroevolution of Augmented Topology).

Chapter 4 is where they propose their methodology for training GANS using 2 new types. The first is Genetic neuroevolutionary training of deep convolutional GANs (GAGAN) and the second is Cultural neuroevolutionary training of deep convolutional GANs (CAGAN). These two GAN's utalize evolution for not just the hyper-parameters, but the entire layout of the network, from number of hidden layers, to the number of nodes per layer, and the activation/loss functions of each later.

GAGAN work off of traditional evolution where a population of randomly generated Generators and Discriminators is created, and the best performing (lowest FID score) are the ones that create off spring via Crossover, while a random sample is chosen for Mutation. These become the offspring used in the next iteration.

CAGAN work differently, instead using two search spaces, population and belief(culture) The population space contains the Generator and Discriminator, while the belief space contains the 3 types of knowledge, Situation, Domain, and Normative. These three types are influenced by the population, which intern influences the next generation of the population Situational contains the best FID scoring individual, Domain contains the best solutions since evolution began, and Normative has a set of intervals that define the range of what is believed to be a good search space for each dimension of the problem.

Chapters 5-7 discusses the experiment using MNIST, Fashion MNSIT, and Stroke Faces datasets, each in grayscale upscaled to 64x64. In each of the 3 datasets, they were compaired against traditional GAN's DCGAN, and COEGAN's.  The GAGAN and CAGAN performed better than the vanilla gan and the DCGAN in all 3 trials. and were on par with the COEGAN.  While the new GAN's did not outperform the COEGAN, they did show improvement over other methods. A main limitation of their study was computing power, as the COEGAN was trained with a higher population (10) vs the GA/CAGAN's (7).  Overall the new GAN's proposed have a high computational complexity and did not outperform the COEGAN, but nonetheless have created a new area for further research into tuning the parameters and further modifications to allow the CA/GAGAN's to self-optimize the network as it is trained.
