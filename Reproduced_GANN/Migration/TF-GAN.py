
# coding: utf-8

# In[1]:


import tensorflow as tf
assert int(tf.__version__[0]) >= 2


# In[2]:


import time
from tensorflow import keras
from tensorflow.keras import layers
get_ipython().system('pip install tensorflow-gan')
import tensorflow_gan as tfgan
import os
import numpy as np
import csv
import pandas as pd
import functools
import matplotlib.pyplot as plt
import math

from IPython import display


# In[3]:



# ## CONSTANTS

# In[4]:


np.set_printoptions(precision=3, suppress=True)
pd.options.display.float_format = "{:,.0f}".format


# In[5]:


LABEL_COLUMN = 'income'
LABELS = [0, 1]
EPOCHS = 5000
BATCH_SIZE = 128
NUM_SAMPLES_TO_GENERATE = 16
NORMALIZED_OUTPUT = True
CSV_COLUMNS = ['age', 'workclass', 'fnlwgt', 'education', 'education.num', 'marital.status', 'occupation', 'relationship', 'race', 'sex', 'captial.gain', 'capital.loss', 'hours.per.week', 'native.country', 'income']
NUMERIC_FEATURES = ['age', 'fnlwgt', 'education.num', 'captial.gain', 'capital.loss', 'hours.per.week']
CATEGORICAL_FEATURES = ['income','workclass', 'education', 'marital.status', 'occupation', 'relationship', 'race', 'sex', 'native.country']


# ## Download & Create Pandas DataFrame

# In[6]:


TRAIN_DATA_URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data"
TEST_DATA_URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.test"


# In[7]:


if(os.path.isfile('raw_train_data.csv')):
    raw_train_data = pd.read_csv('raw_train_data.csv', index_col=0)
else:
    print("Downloading Training File...")
    raw_train_data = pd.read_csv(TRAIN_DATA_URL, names=CSV_COLUMNS).drop(labels=CATEGORICAL_FEATURES, axis=1)
    raw_train_data.to_csv('raw_train_data.csv')
    print("File Downloaded")
    
if(os.path.isfile('raw_test_data.csv')):
    raw_test_data = pd.read_csv('raw_test_data.csv', index_col=0)
else:
    print("Downloading Testing File...")
    raw_test_data = pd.read_csv(TEST_DATA_URL, names=CSV_COLUMNS, skiprows=[0]).drop(labels=CATEGORICAL_FEATURES, axis=1)
    raw_test_data.to_csv('raw_test_data.csv')
    print("File Downloaded")


# In[8]:


# A utility method to create a tf.data dataset from a Pandas Dataframe
def df_to_dataset(dataframe, shuffle=True, batch_size=32):
  dataframe = dataframe.copy()           # makes a copy of the dataframe so as not to modify the original
  #labels = None #dataframe.pop(LABEL_COLUMN)   # seperates out the labels column
  ds = tf.data.Dataset.from_tensor_slices((dict(dataframe)))    # creates the TF dataset object
  if shuffle:            # if shuffle is set to True, it sets the dataset to shuffle the samples in a batch
    ds = ds.shuffle(buffer_size=len(dataframe))
  ds = ds.batch(batch_size)    # sets the batch size
  return ds


# ## Normalize Continuous Data

# In[9]:


normalized_train_data = raw_train_data/raw_train_data.max()
normalized_test_data = raw_test_data/raw_test_data.max()
if(os.path.isfile('raw_train_data.csv') == False):
    normalized_test_data.to_csv('normalized_test_data.csv')


# In[10]:


train_dataset = df_to_dataset(normalized_train_data, batch_size=BATCH_SIZE)
test_dataset = df_to_dataset(normalized_test_data, batch_size=BATCH_SIZE)


# In[11]:


# Selects a list of numeric features & packs into a single column
class PackNumericFeatures(object):
  def __init__(self, names):
    self.names = names

  def __call__(self, features):
    numeric_features = [features.pop(name) for name in self.names]
    numeric_features = [tf.cast(feat, tf.float32) for feat in numeric_features]
    numeric_features = tf.stack(numeric_features, axis=-1)
    features['numeric'] = numeric_features

    return features


# In[12]:


train_data=train_dataset.map(PackNumericFeatures(NUMERIC_FEATURES))
test_data=test_dataset.map(PackNumericFeatures(NUMERIC_FEATURES))


# ## TF-GAN

# In[13]:


def input_fn(mode, params):
    assert 'batch_size' in params
    assert 'noise_dims' in params
    bs = params['batch_size']
    nd = params['noise_dims']
    split = 'train' if mode == tf.estimator.ModeKeys.TRAIN else 'test'
    shuffle = (mode == tf.estimator.ModeKeys.TRAIN)
    just_noise = (mode == tf.estimator.ModeKeys.PREDICT)
    
    noise_ds = (tf.data.Dataset.from_tensors(0).repeat().map(lambda _: tf.random.normal([bs, nd])))
    
    if just_noise:
        return noise_ds
    
    if split == 'train':
        census_ds = (train_data.cache().repeat())
    else:
        census_ds = (test_data.cache().repeat())
        
    if shuffle:
        census_ds = census_ds.shuffle(
        buffer_size=10000, reshuffle_each_iteration=True)
        
    census_ds = census_ds.batch(bs, drop_remainder = True).prefetch(
        tf.data.experimental.AUTOTUNE)
    
    return tf.data.Dataset.zip((noise_ds,census_ds))


# In[14]:


params = {'batch_size': BATCH_SIZE, 'noise_dims':6}
with tf.Graph().as_default():
    ds = input_fn(tf.estimator.ModeKeys.TRAIN, params)


# In[ ]:


def _dense(inputs, units, l2_weight):
  return tf.layers.dense(
      inputs, units, None,
      kernel_initializer=tf.keras.initializers.glorot_uniform,
      kernel_regularizer=tf.keras.regularizers.l2(l=l2_weight),
      bias_regularizer=tf.keras.regularizers.l2(l=l2_weight))

def _batch_norm(inputs, is_training):
  return tf.layers.batch_normalization(
      inputs, momentum=0.999, epsilon=0.001, training=is_training)


# In[ ]:


_leaky_relu = lambda net: tf.nn.leaky_relu(net, alpha=0.01)


# In[ ]:


def unconditional_generator(noise, mode, weight_decay=2.5e-5):
    is_training = (mode == tf.estimator.ModeKeys.TRAIN)

    net = _dense(noise, 12, weight_decay)
    net = _batch_norm(net, is_training)
    net = _leaky_relu(net)

    net = _dense(net, 12, weight_decay)
    net = _leaky_relu(net)
    
    net = _dense(net, 12, weight_decay)
    net = _leaky_relu(net)

    net = _dense(net, 12, weight_decay)
    net = _leaky_relu(net)

    net = tf.sigmoid(net)

    return net


# In[ ]:


def unconditional_discriminator(sample, unused_conditioning, mode, weight_decay=2.5e-5):
    del unused_conditioning
    is_training = (mode == tf.estimator.ModeKeys.TRAIN)

    net = _dense(sample, 12, weight_decay)
    net = _batch_norm(net, is_training)
    net = _leaky_relu(net)
    
    net = _dense(net, 12, weight_decay)
    net = _leaky_relu(net)
    
    net = _dense(net, 12, weight_decay)
    net = _leaky_relu(net)

    net = _dense(net, 12, weight_decay)
    net = _leaky_relu(net)

    net = _dense(net, 1, weight_decay)

    return net


# In[ ]:


def get_eval_metric_ops_fn(gan_model):
    real_data_logits = tf.reduce_mean(gan_model.discriminator_real_outputs)
    gen_data_logits = tf.reduce_mean(gan_model.discriminator_gen_outputs)
   
    return {
    'real_data_logits': tf.metrics.mean(real_data_logits),
    'gen_data_logits': tf.metrics.mean(gen_data_logits),
    }


# In[ ]:


train_batch_size = 32 #@param
noise_dimensions = 64 #@param
generator_lr = 0.001 #@param
discriminator_lr = 0.0002 #@param

def gen_opt():
    gstep = tf.train.get_or_create_global_step()
    base_lr = generator_lr
    # Halve the learning rate at 1000 steps.
    lr = tf.cond(gstep < 1000, lambda: base_lr, lambda: base_lr / 2.0)
    return tf.train.AdamOptimizer(lr, 0.5)

gan_estimator = tfgan.estimator.GANEstimator(
    generator_fn=unconditional_generator,
    discriminator_fn=unconditional_discriminator,
    generator_loss_fn=tfgan.losses.wasserstein_generator_loss,
    discriminator_loss_fn=tfgan.losses.wasserstein_discriminator_loss,
    params={'batch_size': train_batch_size, 'noise_dims': noise_dimensions},
    generator_optimizer=gen_opt,
    discriminator_optimizer=tf.optimizers.Adam(discriminator_lr, 0.5),
    get_eval_metric_ops_fn=get_eval_metric_ops_fn)


# In[ ]:




# Disable noisy output.
tf.autograph.set_verbosity(0, False)

import time
steps_per_eval = 500 #@param
max_train_steps = 5000 #@param
batches_for_eval_metrics = 100 #@param

# Used to track metrics.
steps = []
real_logits, fake_logits = [], []
#real_mnist_scores, mnist_scores, frechet_distances = [], [], []

cur_step = 0
start_time = time.time()
while cur_step < max_train_steps:
    next_step = min(cur_step + steps_per_eval, max_train_steps)

    start = time.time()
    gan_estimator.train(input_fn, max_steps=next_step)
    steps_taken = next_step - cur_step
    time_taken = time.time() - start
    print('Time since start: %.2f min' % ((time.time() - start_time) / 60.0))
    print('Trained from step %i to %i in %.2f steps / sec' % (
      cur_step, next_step, steps_taken / time_taken))
    cur_step = next_step

    # Calculate some metrics.
    metrics = gan_estimator.evaluate(input_fn, steps=batches_for_eval_metrics)
    steps.append(cur_step)
    real_logits.append(metrics['real_data_logits'])
    fake_logits.append(metrics['gen_data_logits'])
    #real_mnist_scores.append(metrics['real_mnist_score'])
    #mnist_scores.append(metrics['mnist_score'])
    #frechet_distances.append(metrics['frechet_distance'])
    print('Average discriminator output on Real: %.2f  Fake: %.2f' % (
      real_logits[-1], fake_logits[-1]))
    #print('Inception Score: %.2f / %.2f  Frechet Distance: %.2f' % (
      #mnist_scores[-1], real_mnist_scores[-1], frechet_distances[-1]))

    # Vizualize some images.
    iterator = gan_estimator.predict(
      input_fn, hooks=[tf.train.StopAtStepHook(num_steps=21)])
    try:
        imgs = np.array([next(iterator) for _ in range(20)])
    except StopIteration:
        pass
    tiled = tfgan.eval.python_image_grid(imgs, grid_shape=(2, 10))
    plt.axis('off')
    plt.imshow(np.squeeze(tiled))
    plt.show()

